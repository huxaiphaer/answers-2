# answers-2

## Qn 1

According to the solution I provided earlier, I was first getting the user details , this can also be elaborated more in SQL e.g

```
SELECT * FROM USER where pk=any_id;

```

then after I edit the results of the user , of which Django uses a queryset to ensure that the users details (e.g first name ) are changed.

But this can be improved with the usage of the Django auth  `update()` function which can be able to save the running time thus improving the optimizatio compared to my first solution.

Below is how I can update the user with one line plus improved optimization :

```
User.objects.filter(pk=pk).update(first_name='any_name')
```

The above query in SQL can be like this :

```
UPDATE table_name SET first_name = 'any_name'  WHERE pk=pk;
```

In conclusion,  with the above solution, yes we can be able to update the **first name** with one query DB, regarding the 
business logic impact, it will reduce on the boilerplate thus improving on the performance (optimization).


## Qn 2.

*grps* will look like this when it comes to changing the query to return a flat python list of group name strings :

```
grps  = User.groups.values_list(name="name", flat=True)
list(grps)
```

The above query, uses `value_list()` to give us room to add an attribute of `flat` to toggle it to true, thus helping to achieve the flat python list,
then finally , we listify our query which has tuples to come up with the exact output.

In a nutshell, this above query also increase on the performance of our system since we have minimized on the number of queries and yes with my previous soultion, the above code shows that it can one query.

## Qn 3.

The impact of what I had implemented earlier will return an empty queryset list because I was only dealing with 
the **DateField** while searching on the **DateTimeField**.

To make it right, I would change the **DateField** to **DateTimeField** before filtering, then it should be able to return a queryset list if there's data
Below is the updated code  :

```
first_date = datetime.date(2016, 1, 1)
last_date = datetime.date(2016, 4, 1)
users_date_joined = User.objects.filter(date_joined__range=(first_date, last_date)).count()

```




